package br.ucsal.testequalidade20191.locadora;

import br.ucsal.testequalidade20191.locadora.dominio.Cliente;

public class ClienteBuilder {


	private static final String CPF_DEFAULT = "000";
	private static final String NOME_DEFAULT = "Paulo";
	private static final String TELEFONE_DEFAULT = "1234";

	private String cpf  = CPF_DEFAULT;
	private String nome = NOME_DEFAULT;
	private String telefone = TELEFONE_DEFAULT;

	private ClienteBuilder() {

	}

	public static ClienteBuilder umCliente() {
		return new ClienteBuilder();
	}

	public ClienteBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	public ClienteBuilder comCpf(String cpf) {
		this.cpf = cpf;
		return this;		
	}
	public ClienteBuilder comTelefone(String telefone) {
		this.telefone = telefone;
		return this;	

	}
	public Cliente build() {
		Cliente cliente = new Cliente();		
		cliente.setNome(nome);
		cliente.setCpf(cpf);
		cliente.setTelefone(telefone);
		return cliente;
	}
}
