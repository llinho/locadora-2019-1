package br.ucsal.testequalidade20191.locadora;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.ucsal.testequalidade20191.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20191.locadora.dominio.Cliente;
import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20191.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20191.locadora.persistence.VeiculoDAO;

public class LocacaoBOUnitarioTest {



	/**
	 * Locar, para um cliente cadastrado, um veículo disponível. Método: public
	 * static void locarVeiculos(String cpfCliente, List<String> placas, Date
	 * dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {
	 *
	 * Observação: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * @throws Exception
	 */

	private ClienteDAO clienteDAOMock;

	private VeiculoDAO veiculoDAOMock;

	private LocacaoDAO locacaoDAOMock;

	private LocacaoBO locacaoBO;

	@Before
	public void setup() {
		clienteDAOMock = Mockito.mock(ClienteDAO.class);
		veiculoDAOMock = Mockito.mock(VeiculoDAO.class);
		locacaoDAOMock = Mockito.mock(LocacaoDAO.class);
		locacaoBO = new LocacaoBO(clienteDAOMock,veiculoDAOMock,locacaoDAOMock);
	}
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {		
		String cpf = "321";

		Cliente cliente1 = ClienteBuilder.umCliente().build();
		Mockito.doReturn(cliente1).when(clienteDAOMock).obterPorCpf(cpf);		

	}

	@Test
	public void locarVeiculosTest(String cpfCliente, List<String> placas, Date dataLocacao, Integer quantidadeDiasLocacao){

	}
	@Test
	public void testarVeiculosDisponiveis() {

		Cliente clienteDisponivel = ClienteBuilder.umCliente().build();


		locacaoBO.locarVeiculos(cpfCliente, placas, dataLocacao, quantidadeDiasLocacao);


	}

}
