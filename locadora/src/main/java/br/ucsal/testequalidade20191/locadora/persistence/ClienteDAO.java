package br.ucsal.testequalidade20191.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.testequalidade20191.locadora.dominio.Cliente;
import br.ucsal.testequalidade20191.locadora.exception.ClienteNaoEncontradoException;

public class ClienteDAO {

	private List<Cliente> clientes = new ArrayList<>();

	public Cliente obterPorCpf(String cpf) throws ClienteNaoEncontradoException {
		System.out.println("entrou 1");
		for (Cliente cliente : clientes) {
			if (cliente.getCpf().equalsIgnoreCase(cpf)) {
				return cliente;
			}
		}
		throw new ClienteNaoEncontradoException();
	}
	
	public void insert(Cliente cliente){
		clientes.add(cliente);
	}

}
